FROM ubuntu:16.04

# inspired by rabits/qt which we use for the gcc toolkit
MAINTAINER Aleix Pol <aleixpol@kde.org>

ARG QT_VERSION=5.12.1
ARG QT_TAG=v5.12.1
ARG NDK_VERSION=r18b
ARG SDK_PLATFORM=android-21
ARG SDK_BUILD_TOOLS=28.0.2
ARG SDK_PACKAGES="tools platform-tools"

ENV DEBIAN_FRONTEND noninteractive
ENV QT_PATH /opt/Qt
ENV QT_ANDROID ${QT_PATH}/${QT_VERSION}/android_armv7
ENV ANDROID_HOME /opt/android-sdk
ENV ANDROID_SDK_ROOT ${ANDROID_HOME}
ENV ANDROID_NDK_ROOT /opt/android-ndk
ENV ANDROID_NDK_HOST linux-x86_64
ENV ANDROID_NDK_PLATFORM ${SDK_PLATFORM}
ENV PATH ${QT_ANDROID}/bin:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools:${PATH}

# Install updates & requirements:
#  * unzip - unpack platform tools
#  * git, openssh-client, ca-certificates - clone & build
#  * locales, sudo - useful to set utf-8 locale & sudo usage
#  * curl - to download Qt bundle
#  * make, default-jdk, ant - basic build requirements
#  * libsm6, libice6, libxext6, libxrender1, libfontconfig1, libdbus-1-3 - dependencies of Qt bundle run-file
#  * libc6:i386, libncurses5:i386, libstdc++6:i386, libz1:i386 - dependencides of android sdk binaries
RUN dpkg --add-architecture i386 && apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    unzip \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    make \
    default-jdk \
    ant \
    build-essential \
    python \
    libsm6 \
    libice6 \
    libxext6 \
    libxrender1 \
    libfontconfig1 \
    libdbus-1-3 \
    libc6:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    libz1:i386 \

    #build dependencies
    libxml-simple-perl \
    libjson-perl \
    ninja-build \
    build-essential \
    gperf gettext \
    python3 python3-paramiko python3-lxml python3-yaml \
    bison flex \
    ruby wget \
    #for dbus/expat
    automake libtool autoconf autoconf-archive pkg-config \
    #for translations
    subversion gnupg2 \
    && apt-get -qq clean

# Download & unpack android SDK
RUN curl -Lo /tmp/sdk-tools.zip 'https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip' \
    && mkdir -p /opt/android-sdk && unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk && rm -f /tmp/sdk-tools.zip \
    && yes | sdkmanager --licenses && sdkmanager --verbose "platforms;${SDK_PLATFORM}" "build-tools;${SDK_BUILD_TOOLS}" ${SDK_PACKAGES}

# Download & unpack android NDK
RUN mkdir /tmp/android && cd /tmp/android && curl -Lo ndk.zip "https://dl.google.com/android/repository/android-ndk-${NDK_VERSION}-linux-x86_64.zip" \
    && unzip -q ndk.zip && mv android-ndk-* $ANDROID_NDK_ROOT && chmod -R +rX $ANDROID_NDK_ROOT \
    && rm -rf /tmp/android

RUN cd && git clone git://code.qt.io/qt/qt5.git --single-branch --branch ${QT_TAG} && \
    cd qt5 && \
    ./init-repository && \
    ./configure -xplatform android-clang --disable-rpath -nomake tests -nomake examples -android-ndk /opt/android-ndk -android-sdk /opt/android-sdk -skip qttranslations -skip qtserialport -skip qtwebengine -no-warnings-are-errors -opensource -confirm-license -c++std c++14 -prefix $QT_ANDROID && \
    make -j`nproc` && \
    make -j`nproc` install && \
    cd .. && rm -rf qt5

# Reconfigure locale
RUN locale-gen en_US.UTF-8 && dpkg-reconfigure locales

# Add group & user
RUN groupadd -r user && useradd --create-home --gid user user && echo 'user ALL=NOPASSWD: ALL' > /etc/sudoers.d/user

##########################

RUN chmod a+w /opt/

USER user
WORKDIR /home/user
ENV HOME /home/user

RUN mkdir /opt/kdeandroid-deps
RUN mkdir /opt/nativetooling

ENV ANDROID_NDK $ANDROID_NDK_ROOT
COPY gitconfig $HOME/.gitconfig

RUN mkdir -p /opt/cmake \
    && curl -Lo /tmp/cmake.sh https://cmake.org/files/v3.12/cmake-3.12.4-Linux-x86_64.sh \
    && bash /tmp/cmake.sh --skip-license --prefix=/opt/cmake --exclude-subdir \
    && rm /tmp/cmake.sh
ENV PATH="/opt/cmake/bin:${PATH}"

# makes sure gradle is downloaded, otherwise it will be downloaded every time
RUN $QT_ANDROID/src/3rdparty/gradle/gradlew

# compile kf5 tooling
RUN cd && git clone git://code.qt.io/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase \
    && ./configure -prefix /opt/nativetooling -opensource -confirm-license -no-gui -release -optimize-size -nomake tests -nomake examples -no-feature-concurrent \
    && make -j`nproc` && make install && rm -rf ~/qtbase

# Replace androiddeployqt with dev, which has a bunch of patches we need (at least until Qt 5.12)
RUN cd && mkdir androiddeployqt && cd androiddeployqt \
    && wget https://code.qt.io/cgit/qt/qtbase.git/plain/src/tools/androiddeployqt/main.cpp?h=dev -O main.cpp \
    && echo "cmake_minimum_required(VERSION 3.11)\n\
            find_package(Qt5Core REQUIRED)\n\
            add_executable(androiddeployqt main.cpp)\n\
            target_link_libraries(androiddeployqt Qt5::Core)\n" > CMakeLists.txt && cmake . -DQt5Core_DIR=/opt/nativetooling/lib/cmake/Qt5Core && make && sudo cp androiddeployqt /opt/Qt/${QT_VERSION}/android_armv7/bin/androiddeployqt \
    && cd && rm -rf androiddeployqt

COPY build-cmake-native /opt/helpers/
RUN /opt/helpers/build-cmake-native extra-cmake-modules kde:extra-cmake-modules -DCMAKE_INSTALL_PREFIX=/opt/kdeandroid-deps -DCMAKE_PREFIX_PATH="$QT_ANDROID;/opt/kdeandroid-deps"
RUN /opt/helpers/build-cmake-native kconfig kde:kconfig         -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DCMAKE_PREFIX_PATH=/opt/kdeandroid-deps/ -DBUILD_TESTING=OFF -DQt5_DIR=/opt/nativetooling/lib/cmake/Qt5 -DKCONFIG_USE_GUI=OFF
RUN /opt/helpers/build-cmake-native kcoreaddons kde:kcoreaddons -DCMAKE_INSTALL_PREFIX=/opt/nativetooling -DBUILD_SHARED_LIBS=OFF -DCMAKE_PREFIX_PATH=/opt/kdeandroid-deps/ -DBUILD_TESTING=OFF -DQt5Core_DIR=/opt/nativetooling/lib/cmake/Qt5Core -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Widgets=ON

COPY build-standalone build-cmake /opt/helpers/
ENV STANDALONE_CC=clang
ENV STANDALONE_CXX=clang++
ENV STANDALONE_EXTRA=--stl=libc++
ENV ANDROID_NDK_TOOLCHAIN_PREFIX=arm-linux-androideabi

#build expat
RUN cd && git clone https://github.com/libexpat/libexpat.git --single-branch && cd libexpat/expat \
    && /opt/helpers/build-standalone "./buildconf.sh && ./configure --prefix=/opt/kdeandroid-deps --host=${ANDROID_NDK_TOOLCHAIN_PREFIX} && cd lib && make -j`nproc` && make install" \
    && cd ../.. && rm -rf libexpat

#build dbus
RUN EXTRA_CMAKE_SUBDIR=cmake GIT_EXTRA="--branch dbus-1.12.10" /opt/helpers/build-cmake dbus git://anongit.freedesktop.org/git/dbus/dbus -DEXPAT_INCLUDE_DIR=/opt/kdeandroid-deps/include -DEXPAT_LIBRARY=/opt/kdeandroid-deps/lib/libexpat.so

#build qtdbus
RUN unset ANDROID_NDK_TOOLCHAIN_VERSION \
    && sudo sed -i.bak "s|#define QT_NO_DBUS|//#define QT_NO_DBUS //we install it separate from the SDK|g" $QT_ANDROID/include/QtCore/qconfig.h \
    && sudo sed -i.bak "s/QT_FEATURE_dbus -1/QT_FEATURE_dbus 1/g" $QT_ANDROID/include/QtCore/${QT_VERSION}/QtCore/private/qconfig_p.h \
    && sudo sed -i.bak "s/QT_FEATURE_dbus_linked -1/QT_FEATURE_dbus_linked 1/g" $QT_ANDROID/include/QtCore/${QT_VERSION}/QtCore/private/qconfig_p.h \
    && cd && git clone git://code.qt.io/qt/qtbase.git --single-branch --branch ${QT_TAG} && cd qtbase/src/dbus && qmake && make -j`nproc` && sudo make install && cd && rm -rf qtbase \
    && sudo ln -s /opt/nativetooling/bin/qdbusxml2cpp $QT_ANDROID/bin/ \
    && sudo ln -s /opt/nativetooling/bin/qdbuscpp2xml $QT_ANDROID/bin/

# needs to be after building qt, otherwise it breaks weirdly
ENV QMAKESPEC android-clang

COPY build-qt build-generic build-eigen build-poppler build-openssl target-from-manifest.py target-from-appdata.py build-kde-dependencies build-kde-project skip /opt/helpers/
RUN /opt/helpers/build-cmake libintl-lite https://github.com/j-jorge/libintl-lite.git

USER user
WORKDIR /home/user
ENV HOME /home/user

ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
