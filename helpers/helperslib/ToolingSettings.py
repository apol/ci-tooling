import fnmatch
import copy
import os
import yaml

from typing import Dict, Union, List

from helperslib import Buildable, CommonUtils

# Class to make it convenient to read configs for working with settings for tooling
class Loader(object):

	def __init__(self, project: str, platform: str) -> None:

		self.project = project
		self.platform = platform
		self.config = dict()

		# Initialise the dependnecy resolver
		self.resolver = Buildable.DependencyResolver()

		# Ask the resolver to load the list of projects...
		projectsTreeLocation = os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'projects' )
		self.resolver.loadProjectsFromTree( projectsTreeLocation )

		self.resolverProject = self.resolver.retrieveProject( project )

	# Allow fetching our config
	def __getitem__(self, name: str) -> Union[List, str, Dict]:
		# Do we know about this attribute?
		if name in self.config:
			# Then return it
			return self.config[name]

		# We don't know about it
		raise KeyError

	# Load the given specification file
	def loadSpecificationFile(self, configFileLocation: str):
		# Does the file exist?
		# If it does not, then we don't need to do anything else
		if not os.path.isfile( configFileLocation ):
			return

		# Load the file now
		with open( configFileLocation, 'r' ) as configFile:
			# Parse the YAML file
			projectConfig = yaml.load( configFile )

		entries = []
		for key in projectConfig:
			if fnmatch.fnmatch( self.resolverProject.path, key ):
				entries.append( key )

		# sorting all entries - the entries at the end overwrites the former settings
		# len is efficient, but would fail if we have single letter repo names
		entries.sort( key=len )

		self.config = dict()
		for entry in entries:
			content = projectConfig[entry]
			try:
				self.config.update( content['general'] )
				self.config.update( content[self.platform] ) # try to load platform specific settings
			except KeyError:
			  pass
